# Workshop files for "Inkscape für Einsteiger" / "Inkscape for Noobs"

* [ComicRelief.otf](https://fontlibrary.org/en/font/comic-relief) font by Jeff Davis is licensed under the [SIL Open Font License (OFL)](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web)
* [Inkscape Beginners' Guide PDF file](https://inkscape-manuals.readthedocs.io) is licensed [CC-By-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/), contributors are named within the file
* Inkscape icon is licensed [CC-By-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) by Andrew Michael Fitzsimon, trademarked by the Inkscape project / the Software Freedom Conservancy
* All other content is licensed [CC-By-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) by Maren Hachmann, making use of multiple public domain images from [openclipart.org](https://openclipart.org)
